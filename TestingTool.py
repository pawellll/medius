# -*- coding: utf-8 -*-
"""
Created on Sun May  4 22:51:48 2014

@author: Paweł Pęksa
"""
from selenium import webdriver
import time
import logging


class TestingTool(object):
    
    def __init__(self,logfile):
        """logfile: path for the log"""
        logging.basicConfig(filename=logfile,filemode="w",level=logging.INFO,format="%(asctime)s %(message)s")
        logging.info("Start test")
        self._browser = None
        self._distance = 0
    
    def process(self,url,start,stop,departureTime,day,month,year,ascending):
        """Main test procedure, takes url,start,stop,departureTime,day,month,year,ascending(for sorting) """
        try:  
            logging.info("Start browser")
            self._browser = webdriver.Firefox()
            self._loadPage(url)
            self._setStaions(start,stop)
            self._setDate(departureTime,day,month,year)
            time.sleep(3) # let load 
            self._searchConnections()
            time.sleep(3) # let load 
            self._sort(ascending)
            time.sleep(3) # let load 
            self._clickOdleglosc()
            time.sleep(5) # let load 
            self._getDistance()
            logging.info("Close browser\n")
            self._browser.close()
        except Exception as e:
            logging.info("Test failed. Exception:" + str(e) )
            self._browser.save_screenshot("ssException")
            logging.info("Close browser\n")
            self._browser.close()
     
    
    def _loadPage(self,url):
        """Loads page, takes url"""
        logging.info("Load page "+url)
        self._browser.get(url)
    
        
    def _setStaions(self, start,stop):
        """sets stations, takes start and stop"""
        logging.info("Set stations. From "+start+" to "+stop)
        self._browser.find_element_by_name("REQ0JourneyStopsS0G").send_keys(start)
        self._browser.find_element_by_name("REQ0JourneyStopsZ0G").send_keys(stop)
        
        
    def _setDate(self,departureTime,day,month,year):
        """sets date, takes departureTime,day,month,year"""
        logging.info("Set date and click date: "+departureTime+" "+day+" "+month+" "+year)
        self._browser.find_element_by_css_selector("SPAN.upper.pointer").click() #find calendar
        
        self._setTime(departureTime) 
        self._setDayMonthYear(day,month,year)     
        
        self._browser.find_element_by_css_selector("BUTTON.btn.btn-default.pick-date").click() # accept date
        
        
    def _searchConnections(self):
        """clicks the search button"""
        logging.info("Click the button of search connections")
        self._browser.find_element_by_css_selector("BUTTON#singlebutton.btn.btn-primary").click() # find button "wyszukaj połączenie" and click 
        
        
    def _sort(self,ascending):
        """Clicks "CZAS" and expand connections, sorts ascending(1 click) or not ascending(2 clicks)"""
        logging.info("Sort connections. Ascending:"+str(ascending))
        e = self._browser.find_elements_by_xpath("//th[@data-hide='sphone']") #find elements with data-hide=sphone
        for i in e:
            if i.text=="CZAS":

                if ascending:
                    i.click()
                else:
                    i.click()
                    time.sleep(1)
                    i.click()
                    
                break
            
        time.sleep(1)
        logging.info("Expand connections")
        self._browser.find_element_by_css_selector("SPAN.footable-toggle").click() #find first "rozwiń" and click
        
        
    def _clickOdleglosc(self):
        """Clicks "Odległość" """
        logging.info("Click \"Odległość\"")
        e = self._browser.find_elements_by_xpath("//ul/li/a")
        for i in e:
            if i.text=="ODLEGŁOŚĆ (KM)":
                i.click()
                break


    def _getDistance(self):
        """Gets distance from table"""
        logging.info("Get and save distance")
        km=self._browser.find_elements_by_xpath("//div[@class='info-price']/table") #find table with distance
        for i in km:
            #gets number from table
            for i in i.text.split(): 
                try:
                    exactNumber = float(i)
                except ValueError:
                    pass
        self._distance = exactNumber
        print ("Odległość: ",self._distance)
          
          
    def _setTime(self,departureTime):
        """sets time, takes depratureTime"""
        e=self._browser.find_element_by_css_selector("INPUT.day-hour.form-control") #set time
        e.click()
        e.send_keys(departureTime)   
    
    
    def _setDayMonthYear(self,day,month,year):
        """sets day,month,year, takes day,month,year"""
        self._browser.find_element_by_link_text(str(day)).click() #set day

        while self._browser.find_element_by_css_selector("SPAN.ui-datepicker-month").text != month :
            self._browser.find_element_by_css_selector("A.ui-datepicker-next.ui-corner-all").click() #click next month
        
        actualYear = time.localtime().tm_year;
        if actualYear <= int(year) :
            while self._browser.find_element_by_css_selector("SPAN.ui-datepicker-year").text != year :
                self._browser.find_element_by_css_selector("A.ui-datepicker-next.ui-corner-all").click() #click next month    
        else:
            while self._browser.find_element_by_css_selector("SPAN.ui-datepicker-year").text != year :
                self._browser.find_element_by_css_selector("A.ui-datepicker-prev.ui-corner-all").click() #click prev month        
    